Centri d'Italia 2019


Progetto editoriale

Un viaggio inchiesta per raccontare se e come l'accoglienza si sta trasformando nei territori in conseguenza al Decreto Sicurezza.
3 tappe significative che danno una rappresentazione delle tendenze di fondo che si producono nell'intero paese.
Un lavoro giornalistico con l'obiettivo di rompere il monopolio dell'informazione e della comunicazione sul tema assunto dal Governo, e segnatamente, dal Ministro dell'interno, per documentare con dati e testimonianze ciò che effettivamente accade e accadrà nel prossimo futuro. Dunque il periodo su cui si concentra l'analisi è dal 2018 in avanti, in particolare un confronto tra il prima e dopo la riforma, entrata in vigore a ottobre 2018. 
Si definisce un format comune a tutti i territori, composto di tipologie di dati da raccogliere e testimonianze.
Gli aspetti centrali del nuovo assetto (Decreto Sicurezza + Nuovo Capitolato):
- abolizione della protezione umanitaria: effetti in termini di dinieghi delle richieste di protezione e quindi di irregolari
- trasformazione dello SPRAR in SIPROIMI con accoglienza riservata solo ai titolari di protezione escludendo i richiedenti
- riduzione del pro capite/pro die  

Le domande in cerca di risposte:
- BANDI: bandi in essere 2018; nuovi bandi 2018/19; le nuove regole? quali importi? procedure? quali esiti? bandi deserti? proroghe? ossia nel concreto dal punto di vista amministrativo è entrata in vigore la riforma e come?
- SPESA/RISPARMI: stanziamenti/spesa 2019 rispetto 2018
- ASSEGNATARI: nuovi assegnatari/ assegnatari precedenti, ossia se e come sta cambiando il mercato dell'offerta di servizi?
- CENTRI: variazione del numero, dimensioni, tipologia, dislocazione
- FLUSSI: quali effetti sulle presenze nei centri? dislocazione da SPRAR a CAS/CARA? stime di quanti sono in fuorisciuta verso l'irregolarità, etc.

SELEZIONE TERRITORI
- rilevanza dell'accoglienza nel territorio: n. bandi/contratti, bandi deserti, proroghe, n. centri, presenze nel periodo in esame
- riduzione dei centri/presenze: notizie stampa, altro
- boom della lega: comuni/province in cui ha avuto più successo il discorso anti immigrazione, qual è la situazione? come si evolve?


Dati
Considerata l'impossibilità al momento di fare conto sui dati del Ministero dell'Interno e dello SPRAR-SIPROIMI viene di seguito definito il set minimo di dati su cui impostare il lavoro, il quale, in caso di sviluppi positivi relativi alle richieste di accesso alle banche dati centrali, potrà essere eventualmente arricchito in corso d'opera. 

DATI DI CONTESTO
- sbarchi (andamento)
- domande di protezione e relativi esiti (andamento)
- dati demografici su stranieri in Italia, città oggetto di esame
- dati su sicurezza e reati stranieri
- votazioni amministrative/europee 2019: crescita della lega

Risorse
- Schema di capitolato di gara di appalto per la fornitura di beni e servizi relativo alla gestione e al funzionamento dei centri di prima accoglienza (20/11/2018)
http://www.interno.gov.it/it/amministrazione-trasparente/bandi-gara-e-contratti/schema-capitolato-gara-appalto-fornitura-beni-e-servizi-relativo-alla-gestione-e-funzionamento-dei-centri-prima-accoglienza
ALLEGATO B stima costi medi di riferimento: http://www.interno.gov.it/sites/default/files/allegato_b_stima_costi_medi_di_riferimento.pdf
- Schema analizzato da Ass. in migrazione
https://www.inmigrazione.it/UserFiles/File/Documents/273_Dossier%20appalti%20accoglienza.pdf
- Bandi deserti articoli:
reggio emilia: https://www.ilrestodelcarlino.it/reggio-emilia/cronaca/bando-migranti-cooperative-1.4536678
- https://www.ilfattoquotidiano.it/2018/11/09/migranti-con-il-taglio-dei-35-euro-addio-allintegrazione-centri-saranno-svuotati-del-personale-ce-un-rischio-sicurezza/4752240/